// Cat Facts

var cat = "";
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       cat = xhttp.response;

       var allcat = JSON.parse(cat);
		for(i in allcat.all) {
			var li = document.createElement('li');
			var data = document.createTextNode(allcat.all[i].text);

			li.appendChild(data);
			document.getElementById("catlist").appendChild(li);

		}
    }
};
xhttp.open("GET", "http://127.0.0.1:8887/api/facts/cat.json", true);
xhttp.send();


// Dog Facts

var dog = "";
var dxhttp = new XMLHttpRequest();
dxhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       dog = dxhttp.response;

       var alldog = JSON.parse(dog);
		for(i in alldog.all) {
			var li = document.createElement('li');
			var data = document.createTextNode(alldog.all[i].text);

			li.appendChild(data);
			document.getElementById("doglist").appendChild(li);

		}
    }
};

// Horse Facts

dxhttp.open("GET", "http://127.0.0.1:8887/api/facts/dog.json", true);
dxhttp.send();

var horse = "";
var hxhttp = new XMLHttpRequest();
hxhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       horse = hxhttp.response;

       var allhorse = JSON.parse(horse);
		for(i in allhorse.all) {
			var li = document.createElement('li');
			var data = document.createTextNode(allhorse.all[i].text);

			li.appendChild(data);
			document.getElementById("horselist").appendChild(li);

		}
    }
};
hxhttp.open("GET", "http://127.0.0.1:8887/api/facts/horse.json", true);
hxhttp.send();


// Snail Fact

var snail = "";
var sxhttp = new XMLHttpRequest();
sxhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       snail = sxhttp.response;

       var allsnail = JSON.parse(snail);
		for(i in allsnail.all) {
			var li = document.createElement('li');
			var data = document.createTextNode(allsnail.all[i].text);

			li.appendChild(data);
			document.getElementById("snaillist").appendChild(li);

		}
    }
};
sxhttp.open("GET", "http://127.0.0.1:8887/api/facts/snail.json", true);
sxhttp.send();

$(document).ready(function() {
    $('.close').click(function(e){
       e.preventDefault();
       $(this).parents('.pop').fadeOut();
    });

    $('.show').click(function(){
       $(this).next().fadeIn();
    });
});



